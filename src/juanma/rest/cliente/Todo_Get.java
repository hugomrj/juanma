/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juanma.rest.cliente;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

/**
 *
 * @author hugo
 */
public class Todo_Get {

 
  public void wsapi( String url, String jwt, String root, Integer cant ) 
          throws URISyntaxException, IOException, InterruptedException {    
    

    //String api_url = "https://rapsodiaparaguay.myshopify.com/admin/api/2022-04/variants.json?fields=id,product_id,title,price,inventory_item_id,inventory_quantity&limit=250";

    
    
    HttpClient httpClient = HttpClient.newBuilder().build();

    HttpRequest request = HttpRequest.newBuilder()            
                .uri( new URI(url))
                .setHeader("X-Shopify-Access-Token", jwt)
                .GET()
                .build();


    HttpResponse<String> response = httpClient
            .send(request, HttpResponse.BodyHandlers.ofString() );

   
    
    String body = response.body();
      
    
    //System.out.println(body);    
    //System.out.println("");    
    
    body = body.substring(13, body.length()-2 );
    
    
    if (cant > 0){
        System.out.print(",");
    }
    else{
        System.out.print("{\n" +
            "  \"" + root + "\": [");
    }
    
    System.out.print(body);
    //System.out.println(response.statusCode());
   
    
    String link = response.headers().allValues("link").toString();
    
    //this.siguiente( response.headers().allValues("link").toString(), jwt );
    this.siguiente( link, jwt, root );
     
        
  }    
    
  
  
  
  
  public void siguiente (String link, String jwt, String root) 
          throws URISyntaxException, IOException, InterruptedException{
  
        
        int divisor = link.indexOf(",");

        if (divisor == -1){

            //String link1 = link.get(0);

            int elemento = link.indexOf("rel=\"next\"");
            
            if (elemento > 0){
            
                String link2 = link.substring(2, elemento-3);        

                this.wsapi(link2, jwt, root, 1);
            
            }
            else {            
                System.out.print("  ]\n" +
                "}");
            }
            
            

        }
        else{
        
            
            int elemento = link.indexOf("rel=\"next\"");
            
            String link3 = link.substring(divisor+3, elemento-3); 
            
            this.wsapi(link3, jwt, root, 1);
        
        }


  }
    
}
