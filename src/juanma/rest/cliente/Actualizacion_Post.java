/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juanma.rest.cliente;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 *
 * @author hugo
 */
public class Actualizacion_Post {
   
    
 //public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
  public void wsapi( String url, String jwt, String json) 
          throws URISyntaxException, Exception {    
    
      try {
          
          //Message to send          
          /*
          String body = "{" +
                  "    \"location_id\": 67680567530," +
                  "    \"inventory_item_id\": 45304483479786," +
                  "    \"available_adjustment\": " + cantidad +
                  "}";
          */
                    
          //String api_url = "https://rapsodiaparaguay.myshopify.com/admin/api/2022-04/inventory_levels/adjust.json";
                    
          System.out.println("actualizacion");
          
          HttpClient httpClient = HttpClient.newBuilder().build();
          
          HttpRequest request = HttpRequest.newBuilder()
                  .uri( new URI(url))
                  .POST( HttpRequest.BodyPublishers.ofString(json) )
                  .setHeader("X-Shopify-Access-Token", jwt)
                  .setHeader("Content-Type", "application/json")
                  .build();
          
          
          
          HttpResponse<String> response = httpClient
                  .send(request, HttpResponse.BodyHandlers.ofString() );
          
          
          System.out.println(response.body());
          System.out.println(response.statusCode());
                    
          
          
      } catch (Exception ex) {          
          System.out.println(ex.getCause());     
          throw new Exception(ex);
      }
        
     
        
  }    
    
   
  
 //public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
  public void wsapi( String url, String jwt, 
          String loc, String item_id, String cantidad) 
          throws URISyntaxException, Exception {    
    
      try {
          
          //Message to send          
          
          /*
          String body = "{" +
                  "    \"location_id\": 67680567530," +
                  "    \"inventory_item_id\": 45304483479786," +
                  "    \"available_adjustment\": " + cantidad +
                  "}";
          */
          
          String body = "{" +
                  "    \"location_id\": "+loc+"," +
                  "    \"inventory_item_id\": "+item_id+"," +
                  "    \"available_adjustment\": " + cantidad +
                  "}";
          
          
          
          //String api_url = "https://rapsodiaparaguay.myshopify.com/admin/api/2022-04/inventory_levels/adjust.json";
                    
          System.out.println("actualizacion");
          
          HttpClient httpClient = HttpClient.newBuilder().build();
          
          HttpRequest request = HttpRequest.newBuilder()
                  .uri( new URI(url))
                  .POST( HttpRequest.BodyPublishers.ofString(body) )
                  .setHeader("X-Shopify-Access-Token", jwt)
                  .setHeader("Content-Type", "application/json")
                  .build();
          
          
          
          HttpResponse<String> response = httpClient
                  .send(request, HttpResponse.BodyHandlers.ofString() );
          
          
          System.out.println(response.body());
          System.out.println(response.statusCode());
                    
          
          
      } catch (Exception ex) {          
          System.out.println(ex.getCause());     
          throw new Exception(ex);
      }
        
     
        
  }    
    
      
  
}
