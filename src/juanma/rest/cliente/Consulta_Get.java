/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juanma.rest.cliente;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 *
 * @author hugo
 */
public class Consulta_Get {

 
  public void wsapi( String url, String jwt ) 
          throws URISyntaxException, IOException, InterruptedException {    
    

    //String api_url = "https://rapsodiaparaguay.myshopify.com/admin/api/2022-04/variants/43207536574698.json?ids=faa8c63101c2ed26c07c7f505fe5dea2,66b2f02b138e6c4f30a1e42812fc77fb";

    
    
    HttpClient httpClient = HttpClient.newBuilder().build();

    HttpRequest request = HttpRequest.newBuilder()            
                .uri( new URI(url))
                .setHeader("X-Shopify-Access-Token", jwt)
                .GET()
                .build();


    HttpResponse<String> response = httpClient
            .send(request, HttpResponse.BodyHandlers.ofString() );


    System.out.println(response.body());
    System.out.println(response.statusCode());
        
     
        
  }    
    
    
}
