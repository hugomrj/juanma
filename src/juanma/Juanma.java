/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juanma;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import juanma.rest.cliente.Consulta_Get;
import juanma.rest.cliente.Actualizacion_Post;
import juanma.rest.cliente.Todo_Get;

/**
 *
 * @author hugo
 */
public class Juanma {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws URISyntaxException {
        // TODO code application logic here
                
        String op = args[0];
    
        try {
            
            
            if (op.equals("consulta")){  
                
                String url = args[1];        
                String jwt = args[2];        
                
                Consulta_Get api = new Consulta_Get();
                api.wsapi(url, jwt);
                
            }
            else if (op.equals("actualizacion")){    
                                
                String url = args[1];        
                String jwt = args[2];        
                
                String loc = args[3];        
                String item_id = args[4];        
                String cantidad = args[5];        
                
                Actualizacion_Post api = new Actualizacion_Post();                
                api.wsapi(url, jwt, loc, item_id, cantidad);                
            }
            else if (op.equals("todo")){    
                                
                
                String url = args[1];        
                String jwt = args[2];        
                String root = args[3];        
                

                Todo_Get api = new Todo_Get();                
                api.wsapi( url,jwt, root ,0 );                
            }            
            
            
            /*
            else if (op.equals("actualizacion")){    
                                
                String url = args[1];        
                String jwt = args[2];        
                String json = args[3];        
                
                Actualizacion_Post api = new Actualizacion_Post();                
                api.wsapi(url, jwt, json);                
            }
            */
            
            
            
        } catch (Exception ex) {
            Logger.getLogger(Juanma.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getCause());

        }
        
        
    }
    
}
